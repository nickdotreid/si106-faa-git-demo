from FAA import Airport

try:
	DTW = Airport('DTW')
	LAX = Airport('LAX')
	print "Airport works"
except Exception, e:
	print "Maybe that airport code doesn't exist or something"

if DTW.loaded and LAX.loaded:
	print "I've decided to change this whole program to make a point \nbecause this is what I came up with on no sleep"
	print "---- DTW ------"
	print "The temperature is currently %0.1f" % (DTW.temp)
	print "check_the_wather == %s" % (DTW.check_the_weather())
	print "---- LAX ------"
	print "The temperature is currently %0.1f" % (LAX.temp)
	print "check_the_wather == %s" % (LAX.check_the_weather())
