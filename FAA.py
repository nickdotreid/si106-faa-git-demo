import urllib, urllib2
import json

class Airport:
	def __init__(self, airport_code):
		self.airport_code = airport_code
		self.loaded = False
		self.fetch()

	def fetch(self):
		""" Get data for airport """
		request_url = 'http://services.faa.gov/airport/status/%s?format=json' % (self.airport_code)
		result = urllib2.urlopen(request_url)
		data = json.loads(result.read())
		self.name = data['name']
		self.weather = data['weather']['weather']
		self.temp = self.parse_temp(data['weather']['temp'])
		self.loaded = True
		return True

	def parse_temp(self, s):
		""" Return Celcus Temperatue from FAA string """
		# should look like this: 22.0 F (-5.6 C)
		num_str = s.split(' ')[1].replace("(","").replace(")","")
		try:
			return float(num_str)
		except:
			pass
		return False

	def check_the_weather(self):
		""" Tell me if the weather is nice in that city """
		if self.weather.find('snow') < 0:
			if self.temp > 40 and self.temp < 60:
				return "ok"
			elif self.temp >=60 and self.temp < 70:
				return "Awesome!"
		return "BAD"